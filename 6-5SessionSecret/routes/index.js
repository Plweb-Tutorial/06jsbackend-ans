var express = require("express");
var router = express.Router();
var passport = require("passport");
var middleware = require("../middleware");
var path = require("path");

var sqlite3 = require("sqlite3");
var db = new sqlite3.Database(path.join(__dirname, "..", "cart"));

router.get("/", function(req, res){
    res.render("landing");
});

router.get("/aboutus", function(req, res){
    res.render("aboutus");
});

router.get("/ingredient", function(req, res){
    res.render("ingredient");
});

router.get("/register", function(req, res){
   res.render("register"); 
});

router.post("/register", function(req, res){
    var username = req.body.username;
    var email = req.body.email;
    var password = req.body.password;
    console.log(username, email, password);

    db.serialize(function() {
        db.run('INSERT INTO user VALUES (?, ?, ?)', username, email, password, 
        function(err){
            if(err){
                console.log(err);
                req.flash("error", "該用戶已被註冊");
                return res.redirect("register"); 
            }
            else{
                passport.authenticate("local")(req, res, function(){
                    res.redirect("/"); 
                });
            }
        });
    });
});

router.get("/login", function(req, res){
    res.render("login"); 
 });

//                    ++++++ 2 ++++++
router.post("/login", passport.authenticate("local", 
    {
        successRedirect: "/",
        failureRedirect: "/login",
        failureFlash: '帳號或密碼錯誤'
    }), function(req, res){
});

router.get("/logout", function(req, res){
//  ++++++ 3 ++++++
    req.logout();
    res.redirect("/");
 });

//                    ++++++ 4 ++++++        ++++++ this function is next() in middleware ++++++
router.get("/secret", middleware.isLoggedIn, function(req, res){
   res.send("<h1>登入後才看的到此頁面，登出後無法看</h1><br><a href='/logout'>登出</a>"); 
});

module.exports = router;